# Pascal: DLL load delayed
**Support for DelayLoading of DLLs like VC++6.0 or latest Delphi (delayed)**


- **Base author:**
      - [Hallvard Vassbotn] (http://hallvards.blogspot.com/2008/03/tdm8-delayloading-of-dlls.html)


- **Sample:**

       ![1] (https://github.com/pult/dll_load_delay/blob/master/img/sample.png?raw=true "Sample")